import React, { FC, Fragment } from 'react';
import {
  useTableDataContext,
  useSelectedRowContext,
  TableRowDataProvider,
  TableRowIsSelectedProvider,
} from '@providers';
import { TableDataRow } from '../TableDataRow';

interface TableRowsProps {}

const TableRows: FC<TableRowsProps> = (props) => {
  console.log('TableRows');
  const tableData = useTableDataContext();
  const selectedRows = useSelectedRowContext();

  return (
    <Fragment>
      {tableData.map((data) => (
        <TableRowDataProvider rowData={data}>
          <TableRowIsSelectedProvider
            isSelected={selectedRows.some((s) => s.id === data.id)}
          >
            <TableDataRow>{props.children}</TableDataRow>
          </TableRowIsSelectedProvider>
        </TableRowDataProvider>
      ))}
    </Fragment>
  );
};

export default TableRows;
