import React, { FC } from 'react';
import {
  TableDataColumnsProps,
  TableDataColumnsContext,
} from './TableColumnsContext';

interface TableColumnsProviderProps {
  columns: TableDataColumnsProps[];
}

const TableColumnsProvider: FC<TableColumnsProviderProps> = (props) => {
  console.log('TableColumnsProvider');

  return (
    <TableDataColumnsContext.Provider value={props.columns}>
      {props.children}
    </TableDataColumnsContext.Provider>
  );
};

export default TableColumnsProvider;
