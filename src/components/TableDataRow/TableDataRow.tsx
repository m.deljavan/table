import React, { FC } from 'react';
import {
  useTableDataColumnsContext,
  useTableConfigContext,
  TableCellDataProvider,
  useTableRowDataContext,
  useTableComponentsContext,
  useSetSelectedRowsContext,
} from '@providers';
import { RowCheckbox } from '../RowCheckBox';
import { TableDataCell } from '../TableDataCell';
import { RowRadio } from '../RowRadio';
import _ from 'lodash';

interface TableDataRowProps {}

const TableDataRow: FC<TableDataRowProps> = (props) => {
  console.log('TableDataRow');
  const columns = useTableDataColumnsContext();
  const { selectable, isMultiple } = useTableConfigContext();
  const data = useTableRowDataContext();
  const Components = useTableComponentsContext();
  const setSelectedRow = useSetSelectedRowsContext();

  const handleSelectRow = () => {
    setSelectedRow([data]);
  };

  return (
    <Components.tableRow onClick={handleSelectRow}>
      {selectable && (isMultiple ? <RowCheckbox /> : <RowRadio />)}
      {columns.map((col) => (
        <TableCellDataProvider cellData={data[col.accessor]}>
          <TableDataCell>{props.children}</TableDataCell>
        </TableCellDataProvider>
      ))}
    </Components.tableRow>
  );
};

export default TableDataRow;
