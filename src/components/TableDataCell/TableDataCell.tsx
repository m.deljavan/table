import React, { FC } from 'react';
import { useTableComponentsContext } from '@providers';

interface TableDataCellProps {}

const TableDataCell: FC<TableDataCellProps> = (props) => {
  console.log('TableDataCell');
  const Components = useTableComponentsContext();
  return <Components.tableCell>{props.children}</Components.tableCell>;
};

export default TableDataCell;
