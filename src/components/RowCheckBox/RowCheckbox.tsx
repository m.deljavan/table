import {
  useSetSelectedRowsContext,
  useTableComponentsContext,
  useTableRowDataContext,
  useTableRowIsSelectedContext,
} from '@providers';
import React, { FC } from 'react';

interface RowCheckboxProps {
  checked?: boolean;
}

const RowCheckbox: FC<RowCheckboxProps> = (_) => {
  console.log('RowCheckbox');
  const dataRow = useTableRowDataContext();
  const setSelectedRows = useSetSelectedRowsContext();
  const Components = useTableComponentsContext();
  const checked = useTableRowIsSelectedContext();

  const handleChangeChecked: React.InputHTMLAttributes<
    HTMLInputElement
  >['onChange'] = (e) => {
    const checked = e.target.checked;

    if (checked) {
      setSelectedRows((prev) => [...prev, dataRow]);
      return;
    }

    setSelectedRows((prev) => prev.filter((p) => p.id !== dataRow.id));
  };

  return (
    <Components.checkBox
      type={'checkbox'}
      checked={checked}
      onChange={handleChangeChecked}
    />
  );
};

export default RowCheckbox;
