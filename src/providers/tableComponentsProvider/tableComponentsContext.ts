import { createContext, useContext } from 'react';
import { Components } from '@types';
import { contextError } from '@utils';
import { defaultComponents } from '@constants';

export const TableComponentsContext = createContext<Partial<Components> | null>(
  null
);

export const useTableComponentsContext = () => {
  const components = useContext(TableComponentsContext);

  if (!components) {
    throw contextError('useTableComponentsContext', 'TableComponentsProvider');
  }

  return { ...defaultComponents, ...components };
};
