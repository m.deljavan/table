import React, { FC } from 'react';
import { TableRowDataContext } from './TableRowDataContext';

interface TableProviderProps {
  rowData: Record<string, unknown>;
}

const TableRowProvider: FC<TableProviderProps> = (props) => {
  console.log('TableRowProvider');
  const { rowData } = props;
  return (
    <TableRowDataContext.Provider value={rowData}>
      {props.children}
    </TableRowDataContext.Provider>
  );
};

export default TableRowProvider;
