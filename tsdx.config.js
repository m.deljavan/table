const alias = require('@rollup/plugin-alias');

module.exports = {
  rollup(config) {
    config.plugins.push(
      alias({
        '@utils': './src/utils',
        '@providers': './src/providers',
      })
    );

    return {
      ...config,
      external: (packageName) =>
        !/^(?:@utils | @providers)/.test() && config.external(packageName),
    };
  },
};
