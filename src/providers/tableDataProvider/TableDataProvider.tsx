import React, { FC, useState } from 'react';
import {
  TableDataData,
  TableDataContext,
  TableDataSetContext,
} from './TableDataContext';

interface TableDataProviderProps {
  data: TableDataData[];
}

const TableDataProvider: FC<TableDataProviderProps> = (props) => {
  console.log('TableDataProvider');
  const [data, setData] = useState<TableDataData[]>(props.data);

  return (
    <TableDataSetContext.Provider value={setData}>
      <TableDataContext.Provider value={data}>
        {props.children}
      </TableDataContext.Provider>
    </TableDataSetContext.Provider>
  );
};

export default TableDataProvider;
