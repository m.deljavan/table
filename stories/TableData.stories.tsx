import { storiesOf } from '@storybook/react';
import React, { useState } from 'react';
import {
  TableDataBody,
  TableDataHeader,
  TableRows,
  useTableCellDataContext,
} from '../src';
import TableData from '../src/components/TableData';
import { withKnobs, boolean } from '@storybook/addon-knobs';
const data = Array.from(new Array(200)).map((_, index) => ({
  id: index,
  name: `mda${index}`,
  age: index,
  lastName: `mm`,
}));

const columns = [
  {
    title: 'lastName',
    accessor: 'lastName',
  },
  {
    title: 'name',
    accessor: 'name',
  },
  {
    title: 'name',
    accessor: 'name',
  },
  {
    title: 'name',
    accessor: 'name',
  },
];
storiesOf('table', module)
  .addDecorator(withKnobs)
  .add('mda', () => {
    const [s, setS] = useState(false);
    const [value, setValue] = useState('');
    return (
      <div>
        <input value={value} onChange={(e) => setValue(e.target.value)} />
        <TableData
          data={data}
          selectedRows={[]}
          onChangeSelectedData={() => {}}
          config={{
            selectable: boolean('isSelectable', false),
            isMultiple: boolean('isMultiple', false),
          }}
          tableColumns={columns}
        >
          <TableDataHeader />
          <TableDataBody>
            <TableRows>
              <Cell />
            </TableRows>
          </TableDataBody>
        </TableData>
      </div>
    );
  });

const Cell = () => {
  const data = useTableCellDataContext();
  console.log(data);

  return data as any;
};
