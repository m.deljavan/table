import { createContext, useContext } from 'react';
import { contextError } from '@utils';

export const TableRowIsSelectedContext = createContext<boolean | null>(null);

export const useTableRowIsSelectedContext = () => {
  const isSelected = useContext(TableRowIsSelectedContext);

  if (isSelected === null) {
    throw contextError(
      'useTableRowIsSelectedContext',
      'TableRowIsSelectedProvider'
    );
  }

  return isSelected;
};
