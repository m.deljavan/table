import { createContext, useContext } from 'react';
import { TableConfig } from '@types';

const defaultConfig: TableConfig = {
  selectable: false,
  isMultiple: true,
};
export const TableConfigContext = createContext<Partial<TableConfig>>(
  defaultConfig
);

export const useTableConfigContext = () => {
  const tableConfig = useContext(TableConfigContext);

  return { ...defaultConfig, ...tableConfig };
};
