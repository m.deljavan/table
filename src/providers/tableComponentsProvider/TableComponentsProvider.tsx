import React, { FC } from 'react';
import { Components } from '@types';
import { TableComponentsContext } from './tableComponentsContext';

interface TableComponentsProviderProps {
  components: Partial<Components>;
}

const TableComponentsProvider: FC<TableComponentsProviderProps> = (props) => {
  console.log('TableComponentsProvider');
  const { components } = props;
  return (
    <TableComponentsContext.Provider value={components}>
      {props.children}
    </TableComponentsContext.Provider>
  );
};

export default TableComponentsProvider;
