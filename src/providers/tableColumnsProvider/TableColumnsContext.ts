import { createContext, ReactNode, useContext } from 'react';
import { contextError } from '@utils';
export interface TableDataColumnsProps {
  title: ReactNode;
  accessor: string;
}
export const TableDataColumnsContext = createContext<
  TableDataColumnsProps[] | null
>(null);

export const useTableDataColumnsContext = () => {
  const columns = useContext(TableDataColumnsContext);

  if (!columns) {
    throw contextError(
      'useTableDataColumnsContext',
      'TableDataColumnsProvider'
    );
  }

  return columns;
};
