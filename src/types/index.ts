import { ComponentType } from 'react';

export interface TableConfig {
  selectable: boolean;
  isMultiple: boolean;
}

export interface Components {
  table: 'table' | ComponentType;
  tableHead: 'thead' | ComponentType;
  tableRow: 'tr' | ComponentType;
  tableCell: 'td' | ComponentType;
  tableBody: 'tbody' | ComponentType;
  tableHeadCell: 'th' | ComponentType;
  checkBox: 'input' | ComponentType;
  radio: 'input' | ComponentType;
}
