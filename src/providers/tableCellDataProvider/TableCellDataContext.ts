import { createContext, useContext } from 'react';
import { contextError } from '@utils';

export const TableCellDataContext = createContext<unknown | null>(null);

export const useTableCellDataContext = () => {
  const tableCellData = useContext(TableCellDataContext);

  if (TableCellDataContext === null || TableCellDataContext === undefined) {
    throw contextError('useTableCellDataContext', 'TableCellDataProvider');
  }

  return tableCellData;
};
