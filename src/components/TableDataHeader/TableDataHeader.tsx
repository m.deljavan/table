import React, { FC, memo } from 'react';
import {
  useTableDataColumnsContext,
  useTableComponentsContext,
  useTableConfigContext,
} from '@providers';

interface TableDataHeaderProps {}

const TableDataHeader: FC<TableDataHeaderProps> = () => {
  console.log('TableDataHeader');
  const columns = useTableDataColumnsContext();
  const Components = useTableComponentsContext();
  const { selectable } = useTableConfigContext();
  return (
    <Components.tableHead>
      <Components.tableRow>
        {selectable && <Components.tableHeadCell />}
        {columns.map((col) => (
          <Components.tableHeadCell>{col.title}</Components.tableHeadCell>
        ))}
      </Components.tableRow>
    </Components.tableHead>
  );
};

export default memo(TableDataHeader);
