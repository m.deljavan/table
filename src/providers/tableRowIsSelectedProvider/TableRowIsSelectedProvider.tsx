import React, { memo, PropsWithChildren } from 'react';
import { TableRowIsSelectedContext } from './tableRowIsSelectedContext';

interface TableRowIsSelectedProviderProps {
  isSelected: boolean;
}

const TableRowIsSelectedProvider = (
  props: PropsWithChildren<TableRowIsSelectedProviderProps>
) => {
  console.log('TableRowIsSelectedProvider');
  const { isSelected } = props;
  return (
    <TableRowIsSelectedContext.Provider value={isSelected}>
      {props.children}
    </TableRowIsSelectedContext.Provider>
  );
};

export default memo(
  TableRowIsSelectedProvider,
  (p, n) => p.isSelected === n.isSelected
);
