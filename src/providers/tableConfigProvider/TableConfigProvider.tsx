import React, { FC, useState, useEffect } from 'react';
import { TableConfigContext } from './TableConfigContext';
import { TableConfig } from '@types';

interface TableConfigProviderProps {
  config: Partial<TableConfig>;
}

const TableConfigProvider: FC<TableConfigProviderProps> = (props) => {
  console.log('TableConfigProvider');
  const { children, config } = props;
  const [c, setC] = useState(config);
  // TODO use state to memorize props
  useEffect(() => {
    setC(config);
  }, [...Object.values(config)]);
  return (
    <TableConfigContext.Provider value={c}>
      {children}
    </TableConfigContext.Provider>
  );
};

export default TableConfigProvider;
