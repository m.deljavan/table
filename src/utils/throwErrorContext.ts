export const contextError = (useContextName: string, providerName: string) => {
  return new Error(`${useContextName} must be used within ${providerName}`);
};
