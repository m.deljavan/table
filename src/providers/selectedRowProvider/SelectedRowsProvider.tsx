import React, { FC, useState } from 'react';
import {
  SelectedRowContext,
  SetSelectedRowsContext,
} from './SelectedRowContext';

interface SelectedRowsProviderProps {
  selectedRows: Record<string, unknown>[];
}

const SelectedRowsProvider: FC<SelectedRowsProviderProps> = (props) => {
  console.log('SelectedRowsProvider');
  const [s, setSelectedRows] = useState<Record<string, unknown>[]>(
    props.selectedRows
  );
  return (
    <SetSelectedRowsContext.Provider value={setSelectedRows}>
      <SelectedRowContext.Provider value={s}>
        {props.children}
      </SelectedRowContext.Provider>
    </SetSelectedRowsContext.Provider>
  );
};

export default SelectedRowsProvider;
