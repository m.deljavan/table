import { createContext, useContext, SetStateAction, Dispatch } from 'react';
import { contextError } from '@utils';

export type TableDataData = Record<string, unknown>;

export const TableDataContext = createContext<TableDataData[] | null>(null);

export const useTableDataContext = () => {
  const tableData = useContext(TableDataContext);

  if (!tableData) {
    throw contextError('useTableDataContext', 'TableDataProvider');
  }

  return tableData;
};

export const TableDataSetContext = createContext<Dispatch<
  SetStateAction<TableDataData[]>
> | null>(null);

export const useTableDataSetContext = () => {
  const setTableData = useContext(TableDataSetContext);

  if (!setTableData) {
    throw contextError('useTableDataSetContext', 'TableDataSetProvider');
  }

  return setTableData;
};
