import React, { FC } from 'react';
import {
  useTableRowDataContext,
  useSetSelectedRowsContext,
  useTableComponentsContext,
  useTableRowIsSelectedContext,
} from '@providers';

interface RowRadioProps {}

const RowRadio: FC<RowRadioProps> = (_) => {
  console.log('RowRadio');

  const dataRow = useTableRowDataContext();
  const setSelectedRows = useSetSelectedRowsContext();
  const Components = useTableComponentsContext();
  const checked = useTableRowIsSelectedContext();

  const handleChangeChecked: React.InputHTMLAttributes<
    HTMLInputElement
  >['onChange'] = (e) => {
    const checked = e.target.checked;

    if (checked) {
      setSelectedRows([dataRow]);
      return;
    }

    setSelectedRows([]);
  };

  return (
    <Components.radio
      type={'radio'}
      checked={checked}
      onChange={handleChangeChecked}
    />
  );
};

export default RowRadio;
