import {
  TableDataColumnsProps,
  TableConfigProvider,
  TableDataProvider,
  TableColumnsProvider,
  SelectedRowsProvider,
  TableComponentsProvider,
} from '@providers';
import { TableConfig, Components } from '@types';
import React, { FC } from 'react';
import { defaultComponents } from '@constants';
import Table from './Table/Table';

interface TableDataProps {
  data: Record<string, unknown>[];
  tableColumns: TableDataColumnsProps[];
  config?: Partial<TableConfig>;
  onChangeSelectedData: (selectedData: Record<string, unknown>[]) => void;
  selectedRows: Record<string, unknown>[];
  Components?: Partial<Components>;
}

const TableData: FC<TableDataProps> = (props) => {
  console.log('TableData');
  const {
    data,
    tableColumns,
    config = {},
    selectedRows,
    Components = defaultComponents,
  } = props;

  return (
    <TableConfigProvider config={config}>
      <TableComponentsProvider components={Components}>
        <TableDataProvider data={data}>
          <TableColumnsProvider columns={tableColumns}>
            <SelectedRowsProvider selectedRows={selectedRows}>
              <Table>{props.children}</Table>
            </SelectedRowsProvider>
          </TableColumnsProvider>
        </TableDataProvider>
      </TableComponentsProvider>
    </TableConfigProvider>
  );
};

export default TableData;
