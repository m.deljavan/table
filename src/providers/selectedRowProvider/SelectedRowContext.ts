import { createContext, useContext, Dispatch, SetStateAction } from 'react';
import { contextError } from '@utils';

export const SelectedRowContext = createContext<
  Record<string, unknown>[] | null
>(null);

export const useSelectedRowContext = () => {
  const selectedRows = useContext(SelectedRowContext);

  if (!selectedRows) {
    throw contextError('useSelectedRowContext', 'SelectedRowProvider');
  }

  return selectedRows;
};

export const SetSelectedRowsContext = createContext<Dispatch<
  SetStateAction<Record<string, unknown>[]>
> | null>(null);

export const useSetSelectedRowsContext = () => {
  const setSelectedRows = useContext(SetSelectedRowsContext);

  if (!setSelectedRows) {
    throw contextError('useSetSelectedRowsContext', 'SetSelectedRowsProvider');
  }

  return setSelectedRows;
};
