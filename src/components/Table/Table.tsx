import React, { FC } from 'react';
import { useTableComponentsContext } from '@providers';

interface TableProps {}

const Table: FC<TableProps> = (props) => {
  console.log('Table');
  const Components = useTableComponentsContext();

  return <Components.table>{props.children}</Components.table>;
};

export default Table;
