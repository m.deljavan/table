import React, { FC, memo } from 'react';
import { useTableComponentsContext } from '@providers';

interface TableDataBodyProps {}

const TableDataBody: FC<TableDataBodyProps> = (props) => {
  console.log('TableDataBody');
  const Components = useTableComponentsContext();

  return <Components.tableBody>{props.children}</Components.tableBody>;
};

export default memo(TableDataBody, () => true);
