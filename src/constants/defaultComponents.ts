import { Components } from '@types';

export const defaultComponents: Components = {
  table: 'table',
  tableCell: 'td',
  tableHead: 'thead',
  tableRow: 'tr',
  tableBody: 'tbody',
  tableHeadCell: 'th',
  checkBox: 'input',
  radio: 'input',
};
