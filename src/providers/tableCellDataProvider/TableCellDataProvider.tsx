import React, { FC } from 'react';
import { TableCellDataContext } from './TableCellDataContext';

interface TableCellDataProviderProps {
  cellData: unknown;
}

const TableCellDataProvider: FC<TableCellDataProviderProps> = (props) => {
  console.log('TableCellDataProvider');
  const { cellData } = props;
  return (
    <TableCellDataContext.Provider value={cellData}>
      {props.children}
    </TableCellDataContext.Provider>
  );
};

export default TableCellDataProvider;
