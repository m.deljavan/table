import { createContext, useContext } from 'react';
import { contextError } from '@utils';

export const TableRowDataContext = createContext<Record<
  string,
  unknown
> | null>(null);

export const useTableRowDataContext = () => {
  const tableRowData = useContext(TableRowDataContext);

  if (!tableRowData) {
    throw contextError('useTableRowDataContext', 'TableRowDataProvider');
  }

  return tableRowData;
};
